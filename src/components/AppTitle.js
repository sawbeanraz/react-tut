import React, { Component } from 'react';



class AppTitle extends Component {
  render() {
    return (
      <div className="App-Title">
        <h1>{this.props.componentTitle.title}</h1>
        <p>{this.props.componentTitle.description}</p>
      </div>
    );
  }
}


export default AppTitle;